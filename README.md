# ToDoApp

ToDoApp is a task management application that allows users to add, edit, delete, and view their tasks. It also provides functionality for tracking the user's action history and filtering tasks by priority.

## Project Structure

The ToDoApp project is structured as follows:

- **BusinessLayer**: Contains the business logic of the application, including services for task and user management, validation, and email notifications.
- **DataAccess**: Houses the data access layer with repositories for interacting with the database, including user, task, and history repositories.
- **Model**: Defines the data models used throughout the application, such as User, TodoItem, and History.
- **ToDoApp.UI.WPF**: The user interface layer implemented with WPF, providing the graphical interface for users to interact with the application.
- **Utils**: (Optional) Any utility classes or methods that are used across the application.

## Programming Principles

### KISS 
The KISS principle states that most systems work best if they are kept simple rather than made complicated. The codebase adheres to this principle by using clear and concise methods and classes.

### DRY 
The DRY principle aims to reduce repetition of information. The code avoids duplication by encapsulating common functionality within methods and classes.
Example: [код](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/blob/main/To-DoList/BusinessLayer/Decorator/EmailDecorator.cs?ref_type=heads#L5-L30)

### Program to Interfaces, Not Implementations
The application uses interfaces (e.g., `IValidator`, `ITodoService`, `IUserService`) to define contracts for classes, promoting loose coupling and high cohesion.

### YAGNI
The YAGNI principle suggests not to add functionality until deemed necessary. The code focuses on core functionality without including unnecessary features.

### DYC 
The code follows this principle by implementing the simplest solution that solves the problem at hand, ensuring that the application is built with minimal complexity.

### Separation of Concerns
The code separates different concerns into different layers (e.g., BusinessLayer, DataAccess, Model) and classes, making it easier to manage and maintain.

### Single Responsibility
Each class in the code has a single responsibility. For example, `TodoService` manages todo items, and `UserService` manages users.

### Open/Closed Principle
The code is open for extension but closed for modification. New functionality can be added without changing existing code.

## Design Patterns

### Command Pattern
The Command pattern is used in the `Command` namespace, where commands are encapsulated as objects. This allows for the parameterization of methods with different commands and the ability to queue, schedule, and undo commands.

### State Pattern
The State pattern is used in the `Model.State` namespace, where an object's behavior is determined by its state. A context object delegates state-specific requests to the encapsulated state object.

### Decorator Pattern
The Decorator pattern is used in the `BusinessLayer.Decorator` namespace, where additional responsibilities can be dynamically added to an object. The `EmailDecorator` class adds email functionality to the `UserService` class.

### Repository Pattern
The Repository pattern is used in the `DataAccess.Repository` namespace, where repositories are used to abstract and encapsulate all access to the data source. This pattern helps to create a clean data access layer.

### ChainofResponsibility
The Chain of Responsibility pattern is used for authentication, where multiple handlers process authentication requests sequentially until successful authentication.Used to create a validator with several levels of checks `Validator.cs`, `IValidator`
### Observer 
The Observer pattern enables real-time updates for tasks, notifying observers (such as UI components) about task changes (`HistoryObserver`,`IHistory` and `IObserver`). 

### Strategy 
The Strategy pattern allows users to prioritize tasks differently based on predefined strategies, enhancing customization and flexibility. Used `PriorityFilterStrategy`

## Refactoring Techniques

### Rename Method
The code uses clear and descriptive method names to improve readability.
For example, how it was done in the `HistoryService` class.
```csharp
    //Before
    public async Task AddAsync(History history)
    {}

    // After
    // Асинхронний метод для додавання історії.
    public async Task AddHistoryAsync(History history)
    {
        await _historyRepository.AddAsync(history);
        await _historyRepository.AddHistoryAsync(history);
        // Після додавання історії, сповіщаємо всіх спостерігачів.
        Notify(history);
    }

    //Before
    public async Task<IEnumerable<History>> GetAllAsync()
    {}

    // After
    // Асинхронний метод для отримання всієї історії.
    public async Task<IEnumerable<History>> GetAllHistoriesAsync()
    {
        return await _historyRepository.GetAllAsync();
        return await _historyRepository.GetAllHistoriesAsync();
    }

    //Before
    public async Task DeleteAllAsync()
    {}

    // After
    // Асинхронний метод для видалення всієї історії.
    public async Task DeleteAllHistoriesAsync()
    {
        await _historyRepository.DeleteAllAsync();
        await _historyRepository.DeleteAllHistoriesAsync();
    }
```

### Extract Method
Large methods are broken down into smaller, more manageable methods. Example: [код](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/blob/main/To-DoList/BusinessLayer/ChainofResponsibility/ValidatorBase.cs?ref_type=heads#L33-L66)

### Inline Method
Unnecessary methods are inlined to simplify the code. During the development stage, it was utilized to simplify code comprehension.

### Inline Temp
Temporary variables are inlined to reduce complexity.Helped a lot to simplify and reduce the code

### Replace Temp with Query
Temporary variables are replaced with methods that return the value, improving readability and maintainability.

### Remove Assignments to Parameters
Assignments to method parameters are removed to adhere to the principle of immutability.

### Substitute Algorithm
More efficient algorithms are used where possible to improve performance.

### Move Field
During the development phase, it was used to simplify code, enhance logic, and ensure field-to-class conformity.


### Consolidate Conditional Expression
Consolidating conditional expression used to streamline and simplify complex conditional statements in code. This process involves combining multiple conditional branches into a single, more concise expression

 ### Extract Variable
 This technique is particularly useful when a complex expression or calculation is used multiple times within a method or when a portion of code serves a specific purpose that can be better encapsulated.Separated field validation into another class `ValidatorBase` Example: [код](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/blob/main/To-DoList/BusinessLayer/ChainofResponsibility/ValidatorBase.cs?ref_type=heads)  

# Code References

- [Command Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/ToDoApp.UI.WPF/Command?ref_type=heads)
- [State Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/Model/State?ref_type=heads)
- [Decorator Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/blob/main/To-DoList/BusinessLayer/Decorator/EmailDecorator.cs?ref_type=heads)
- [Repository Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/DataAccess/Repository?ref_type=heads)
- [ChainofResponsibility Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/BusinessLayer/ChainofResponsibility?ref_type=heads)
- [Strategy Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/BusinessLayer/Strategy?ref_type=heads)
- [Observer Pattern](https://gitlab.com/ipz224_mvyu/lab_06_to-dolist/-/tree/main/To-DoList/BusinessLayer/Observer?ref_type=heads)