﻿namespace ToDoApp.UI.WPF.Command;
// Клас, що реалізує інвокер команд для керування командами.
public class CommandInvoker
{
    private ICommand _command;

    // Метод для встановлення команди, яку потрібно виконати або відмінити.
    public void SetCommand(ICommand command)
    {
        _command = command;
    }

    // Метод для виконання поточної команди.
    public void ExecuteCommand()
    {
        _command.Execute();
    }

    // Метод для скасування поточної команди.
    public void UndoCommand()
    {
        _command.Undo();
    }
}
