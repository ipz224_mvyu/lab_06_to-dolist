﻿using BusinessLayer;
using DataAccess.Repository;
using DataAccess;
using Model;

namespace ToDoApp.UI.WPF.Command;
// Клас, що реалізує команду для додавання нового завдання.
public class AddTodoItemCommand : ICommand
{
    // Завдання, яке буде додано.
    private readonly TodoItem _todoItem;

    // Конструктор, який приймає завдання для додавання.
    public AddTodoItemCommand(TodoItem todoItem)
    {
        _todoItem = todoItem;
    }

    // Метод для виконання команди. Він додає завдання до бази даних.
    public async void Execute()
    {
        var todoService = new TodoService(new TodoRepository(new AppDbContext()));
        await todoService.AddTaskAsync(_todoItem);
    }

    // Метод для скасування команди. У цьому випадку, він не реалізований, оскільки скасування додавання завдання не підтримується.
    public void Undo()
    {
        
    }
}
