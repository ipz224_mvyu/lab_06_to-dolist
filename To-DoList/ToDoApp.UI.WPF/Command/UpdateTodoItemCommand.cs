﻿using BusinessLayer;
using DataAccess.Repository;
using DataAccess;
using Model;

namespace ToDoApp.UI.WPF.Command;
// Клас, що реалізує команду для оновлення завдання.
public class UpdateTodoItemCommand : ICommand
{
    private readonly TodoItem _todoItem;

    // Конструктор, який приймає завдання для оновлення.
    public UpdateTodoItemCommand(TodoItem todoItem)
    {
        _todoItem = todoItem;
    }

    // Метод для виконання команди. Він оновлює завдання в базі даних.
    public async void Execute()
    {
        var todoService = new TodoService(new TodoRepository(new AppDbContext()));
        await todoService.UpdateTaskAsync(_todoItem);
    }

    // Метод для скасування команди. У цьому випадку, він не реалізований, оскільки скасування оновлення завдання не підтримується.
    public void Undo()
    {

    }
}
