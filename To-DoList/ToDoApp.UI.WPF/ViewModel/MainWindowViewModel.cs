﻿using BusinessLayer;
using DataAccess.Repository;
using DataAccess;
using Model;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Strategy;
using BusinessLayer.Observer;
using System.Collections.Generic;

namespace ToDoApp.UI.WPF.ViewModel;

public class MainWindowViewModel : INotifyPropertyChanged
{
    public int UserId { get; set; }// Властивість для зберігання ідентифікатора користувача.
    private readonly ITodoService _todoService;// Сервіс для роботи з завданнями.
    private DateTime _selectedDate;// Вибрана дата для відображення завдань.
    private ITaskFilterStrategy _taskFilterStrategy; // Стратегія фільтрації завдань.
    private readonly List<IObserver> _observers = new List<IObserver>();// Список спостерігачів для оновлення історії.
    private readonly HistoryService _historyService;// Сервіс для роботи з історією.

    // Конструктор MainWindowViewModel, де ініціалізуються сервіси та колекції.
    public MainWindowViewModel()
    {
        Attach(new HistoryObserver());
        _todoService = new TodoService(new TodoRepository(new AppDbContext()));
        Tasks = new ObservableCollection<TodoItem>();
        _historyService = new HistoryService(new HistoryRepository(new AppDbContext()));
    }

    // Метод для приєднання спостерігача до подій.
    public void Attach(IObserver observer)
    {
        _observers.Add(observer);
    }

    // Подія для оновлення властивостей.
    public event PropertyChangedEventHandler PropertyChanged;

    // Метод для виклику події оновлення властивостей.
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    // Властивість для відображення вибраної дати.
    public DateTime SelectedDate
    {
        get => _selectedDate;
        set
        {
            if (_selectedDate == value) return;
            _selectedDate = value;
            OnPropertyChanged();
            _ = LoadTasksForDateAsync();
        }
    }

    // Колекція для відображення завдань.
    public ObservableCollection<TodoItem> Tasks { get; }

    // Колекція для відображення записів історії.
    public ObservableCollection<History> HistoryEntries { get; } = new ObservableCollection<History>();

    // Властивість для зберігання стратегії фільтрації завдань.
    public ITaskFilterStrategy TaskFilterStrategy
    {
        get => _taskFilterStrategy;
        set
        {
            _taskFilterStrategy = value;
            OnPropertyChanged();
            _ = LoadTasksForDateAsync();
        }
    }

    // Асинхронний метод для завантаження завдань для вибраної дати.
    public async Task LoadTasksForDateAsync()
    {
        if (SelectedDate == default)
        {
            Tasks.Clear();
            return;
        }

        var tasks = await _todoService.GetTasksForDateAsync(SelectedDate, UserId);

        if (TaskFilterStrategy != null)
        {
            tasks = TaskFilterStrategy.Filter(tasks).ToList();
        }

        Tasks.Clear();
        foreach (var task in tasks)
        {
            Tasks.Add(task);
        }
    }

    // Асинхронний метод для завантаження історії для поточного користувача.
    public async Task LoadHistoryForCurrentUserAsync()
    {
        var historyEntries = await _historyService.GetHistoryByUserIdAsync(UserId);
        HistoryEntries.Clear();
        foreach (var entry in historyEntries)
        {
            HistoryEntries.Add(entry);
        }
    }

    // Метод для видалення завдання з колекції.
    public void RemoveTask(int taskId)
    {
        var taskToRemove = Tasks.FirstOrDefault(t => t.Id == taskId);
        if (taskToRemove != null)
        {
            Tasks.Remove(taskToRemove);
        }
    }
}