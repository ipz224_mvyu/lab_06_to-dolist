﻿using BusinessLayer;
using DataAccess.Repository;
using DataAccess;
using System.Windows;
using System.Windows.Input;
using ToDoApp.UI.WPF.ViewModel;
using BusinessLayer.Decorator;
using System.Net.Mail;
using System.Net;
using BusinessLayer.ChainofResponsibility;

namespace ToDoApp.UI.WPF.View;

public partial class RegistrationWindow : Window
{
    // Конструктор RegistrationWindow, де ініціалізуються компоненти вікна.
    public RegistrationWindow()
    {
        InitializeComponent();
    }

    // Обробник натискання кнопки "Вийти". Він закриває вікно.
    private void ExitButton_MouseDown(object sender, MouseButtonEventArgs e)
    {
        this.Close();
    }

    // Звернути програму
    private void collapseButton_MouseDown(object sender, MouseButtonEventArgs e)
    {
        this.WindowState = WindowState.Minimized;
    }

    // Переміщення форми
    private void ToolBar_MouseDown(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton == MouseButton.Left)
        {
            this.DragMove();

        }
    }

    private void Logo_MouseDown(object sender, MouseButtonEventArgs e)
    {
        if (e.ChangedButton == MouseButton.Left)
        {
            this.DragMove();
        }
    }

    // Крапки в паролі
    private void OnPasswordChanged(object sender, RoutedEventArgs e)
    {
        if (tb2.Password.Length > 0)
        {
            Watermark.Visibility = Visibility.Collapsed;
        }
        else
        {
            Watermark.Visibility = Visibility.Visible;
        }
    }

    // Обробник натискання кнопки "Зареєструватися". Він перевіряє коректність email та пароля, реєструє користувача та відправляє підтвердження на email.
    private async void Registration_Click(object sender, RoutedEventArgs e)
    {
        var email = tb1.Text;
        var password = tb2.Password;

        Validator validator = new Validator();
        if (!validator.ValidateEmail(email))
        {
            MessageBox.Show("Некоректний email. Будь ласка, введіть правильний email.", "Помилка реєстрації", MessageBoxButton.OK, MessageBoxImage.Error);
            return;
        }
        if (!validator.ValidatePassword(password))
        {
            MessageBox.Show("Не надійний password. Будь ласка, введіть сильний password.", "Помилка реєстрації", MessageBoxButton.OK, MessageBoxImage.Error);
            return;
        }

        var smtpClient = new SmtpClient("smtp.gmail.com")
        {
            Port = 587,
            Credentials = new NetworkCredential("todolist913@gmail.com", "b w c n j s g m p f y k u u i h"),
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network
        };

        var fromMailAddress = new MailAddress("todolist913@gmail.com", "ToDoApp");
        var userService = new EmailDecorator(new UserService(new UserRepository(new AppDbContext())), smtpClient, fromMailAddress);

        var isRegistered = await userService.RegisterNewUserAsync(email, password);

        if (isRegistered)
        {
            MessageBox.Show("Реєстрація пройшла успішно. Тепер ви можете увійти в систему.", "Реєстрація успішна", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        else
        {
            MessageBox.Show("Користувач з таким email вже існує. Будь ласка, спробуйте зареєструватися з іншим email або увійдіть в систему.", "Помилка реєстрації", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    // Обробник натискання кнопки "Увійти". Він перевіряє логін та пароль, у разі успіху переходить до головного вікна.
    private async void Button_Click(object sender, RoutedEventArgs e)
    {
        var email = tb1.Text;
        var password = tb2.Password;

        var userService = new UserService(new UserRepository(new AppDbContext()));

        var isLoggedIn = await userService.LoginAsync(email, password);

        if (isLoggedIn)
        {
            var user = await userService.GetUserByEmailAsync(email);
            if (user != null)
            {
                var mainWindow = new MainWindow();
                var viewModel = (MainWindowViewModel)mainWindow.DataContext;
                viewModel.UserId = user.Id;
                mainWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Невірний логін або пароль. Будь ласка, спробуйте ще раз.", "Помилка входу", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        else
        {
            MessageBox.Show("Невірний логін або пароль. Будь ласка, спробуйте ще раз.", "Помилка входу", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
