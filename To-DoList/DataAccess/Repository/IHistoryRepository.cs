﻿using Model;
namespace DataAccess.Repository;
public interface IHistoryRepository
{
    Task<IEnumerable<History>> GetAllHistoriesAsync();
    Task DeleteAllHistoriesAsync();
    Task AddHistoryAsync(History history);
}
