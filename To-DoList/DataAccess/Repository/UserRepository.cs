﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess.Repository;
// Клас, що реалізує репозиторій для роботи з користувачами.
public class UserRepository : IUserRepository
{
    // Контекст бази даних для взаємодії з таблицею користувачів.
    private readonly AppDbContext _context;

    // Конструктор, який приймає контекст бази даних.
    public UserRepository(AppDbContext context)
    {
        _context = context;
    }

    // Асинхронний метод для отримання користувача за email.
    public async Task<User> GetUserByEmailAsync(string email)
    {
        return await _context.Users.SingleOrDefaultAsync(u => u.Email == email);
    }

    // Асинхронний метод для отримання користувача за ідентифікатором.
    public async Task<User> GetUserByIdAsync(int id)
    {
        return await _context.Users.Include(u => u.TodoItems).FirstOrDefaultAsync(u => u.Id == id);
    }

    // Асинхронний метод для додавання нового користувача.
    public async Task AddUserAsync(User user)
    {
        _context.Users.Add(user);
        await _context.SaveChangesAsync();
    }
}
