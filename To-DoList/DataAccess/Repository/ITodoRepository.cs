﻿using Model;

namespace DataAccess.Repository;

public interface ITodoRepository
{
    Task<IEnumerable<TodoItem>> GetAllTasksAsync();
    Task<TodoItem> GetTaskByIdAsync(int id);
    Task AddTaskAsync(TodoItem todoItem);
    Task UpdateTaskAsync(TodoItem todoItem);
    Task RemoveTaskAsync(int id);
}
