﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess.Repository;
// Клас, що реалізує репозиторій для роботи з завданнями.
public class TodoRepository : ITodoRepository
{
    // Контекст бази даних для взаємодії з таблицею завдань.
    private readonly AppDbContext _context;

    // Конструктор, який приймає контекст бази даних.
    public TodoRepository(AppDbContext context)
    {
        _context = context;
    }

    // Асинхронний метод для отримання всіх завдань.
    public async Task<IEnumerable<TodoItem>> GetAllTasksAsync()
    {
        return await _context.TodoItems.Include(t => t.User).ToListAsync();
    }

    // Асинхронний метод для отримання завдання за ідентифікатором.
    public async Task<TodoItem> GetTaskByIdAsync(int id)
    {
        return await _context.TodoItems.Include(t => t.User).FirstOrDefaultAsync(t => t.Id == id);
    }

    // Асинхронний метод для додавання нового завдання.
    public async Task AddTaskAsync(TodoItem todoItem)
    {
        _context.TodoItems.Add(todoItem);
        await _context.SaveChangesAsync();
    }

    // Асинхронний метод для оновлення існуючого завдання.
    public async Task UpdateTaskAsync(TodoItem todoItem)
    {
        _context.Entry(todoItem).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    // Асинхронний метод для видалення завдання за ідентифікатором.
    public async Task RemoveTaskAsync(int id)
    {
        var todoItem = await _context.TodoItems.FindAsync(id);
        if (todoItem != null)
        {
            _context.TodoItems.Remove(todoItem);
            await _context.SaveChangesAsync();
        }
    }
}
