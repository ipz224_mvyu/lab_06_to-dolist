﻿namespace BusinessLayer.ChainofResponsibility;
// Клас, що реалізує валідатор за допомогою патерну "ланцюжок відповідальності".
public class Validator
{
    // Валідатори для електронної пошти та пароля.
    private ValidatorBase _emailValidator;
    private ValidatorBase _passwordValidator;

    // Конструктор, який ініціалізує ланцюжки валідаторів.
    public Validator()
    {
        // Створюємо ланцюжок валідаторів для електронної пошти.
        _emailValidator = new LengthValidator()
            .AddNextValidator(new FormatValidator());

        // Створюємо ланцюжок валідаторів для пароля.
        _passwordValidator = new LengthValidator()
            .AddNextValidator(new ComplexityValidator());
    }

    // Метод для валідації електронної пошти.
    public bool ValidateEmail(string email)
    {
        return _emailValidator.IsValidEmail(email);
    }

    // Метод для валідації пароля.
    public bool ValidatePassword(string password)
    {
        return _passwordValidator.IsValidPassword(password);
    }
}