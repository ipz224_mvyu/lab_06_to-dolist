﻿namespace BusinessLayer.ChainofResponsibility;
// Абстрактний базовий клас для валідаторів у ланцюгу відповідальності.
public abstract class ValidatorBase : IValidator
{
    // Наступний валідатор в ланцюгу.
    protected ValidatorBase? _nextValidator;

    // Метод для встановлення наступного валідатора в ланцюгу.
    public ValidatorBase AddNextValidator(ValidatorBase nextValidator)
    {
        if (_nextValidator == null)
        {
            _nextValidator = nextValidator;
        }
        else
        {
            GetNextValidator(_nextValidator)?.AddNextValidator(nextValidator);
        }
        return this;

    }

    // Якщо наступний валідатор вже встановлено, знаходимо останній валідатор в ланцюгу та додаємо наступного після нього.
    private ValidatorBase GetNextValidator(ValidatorBase currentValidator)
    {
        while (currentValidator._nextValidator != null)
        {
            currentValidator = currentValidator._nextValidator;
        }
        return currentValidator;
    }

    // Метод для перевірки валідності рядка.
    private bool IsValidMethod(string str)
    {
        if (string.IsNullOrWhiteSpace(str))
            return false;

        return IsValid(str);
    }

    // Метод для перевірки валідності електронної пошти.
    public bool IsValidEmail(string email)
    {
        if (IsValidMethod(email))
        {
            if (_nextValidator != null)
                return _nextValidator.IsValidEmail(email);

            return true;
        }
        return false;
    }

    // Метод для перевірки валідності пароля.
    public bool IsValidPassword(string password)
    {
        if (IsValidMethod(password))
        {
            if (_nextValidator != null)
                return _nextValidator.IsValidPassword(password);

            return true;
        }
        return false;
    }

    // Абстрактний метод для перевірки валідності рядка.
    protected abstract bool IsValid(string email);
}
