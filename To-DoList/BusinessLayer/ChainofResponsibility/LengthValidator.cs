﻿namespace BusinessLayer.ChainofResponsibility;
// Клас, що реалізує валідатор довжини електронної пошти в ланцюгу відповідальності.
public class LengthValidator : ValidatorBase
{
    private readonly int _minLength;
    private readonly int _maxLength;
    // Конструктор, який приймає параметри для валідації довжини електронної пошти.
    public LengthValidator(int minLength = 6, int maxLength = 50)
    {
        _minLength = minLength;
        _maxLength = maxLength;
    }
    // Метод для перевірки валідності електронної пошти на основі її довжини.
    protected override bool IsValid(string email)
    {
        if (email.Length < _minLength || email.Length > _maxLength)
            return false;

        if (_nextValidator != null)
            return _nextValidator.IsValidEmail(email);

        return true;
    }
}
