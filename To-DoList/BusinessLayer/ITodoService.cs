﻿using Model;

namespace BusinessLayer;

public interface ITodoService
{
    Task<TodoItem> GetTaskByIdAsync(int id);
    Task AddTaskAsync(TodoItem todoItem);
    Task UpdateTaskAsync(TodoItem todoItem);
    Task RemoveTaskAsync(int id);
    Task<IEnumerable<TodoItem>> GetTasksForDateAsync(DateTime date, int id);
}
