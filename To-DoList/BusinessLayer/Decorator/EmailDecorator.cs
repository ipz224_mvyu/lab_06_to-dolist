﻿using Model;
using System.Net.Mail;

namespace BusinessLayer.Decorator;
// Клас, що реалізує декоратор для відправки електронної пошти під час реєстрації користувача.
public class EmailDecorator : IUserService
{
    // Базовий сервіс для роботи з користувачами.
    private readonly IUserService _userService;

    // SMTP-клієнт для відправки електронної пошти.
    private readonly SmtpClient _smtpClient;

    // Адреса електронної пошти відправника.
    private readonly MailAddress _fromMailAddress;

    // Конструктор, який приймає сервіс для роботи з користувачами, SMTP-клієнт та адресу відправника.
    public EmailDecorator(IUserService userService, SmtpClient smtpClient, MailAddress fromMailAddress)
    {
        _userService = userService;
        _smtpClient = smtpClient;
        _fromMailAddress = fromMailAddress;
    }

    // Асинхронний метод для реєстрації користувача та відправки електронної пошти з підтвердженням.
    public async Task<bool> RegisterNewUserAsync(string email, string password)
    {
        var result = await _userService.RegisterNewUserAsync(email, password);

        // Якщо реєстрація пройшла успішно, відправляємо електронну пошту.
        if (result)
        {
            var toAddress = new MailAddress(email);
            using (var mailMessage = new MailMessage(_fromMailAddress, toAddress))
            {
                mailMessage.Subject = "Реєстрація успішна";
                mailMessage.Body = $"Ваш Email: {email}\nВаш пароль: {password}";
                _smtpClient.Send(mailMessage);
            }
        }

        return result;
    }

    public async Task<User> GetUserByIdAsync(int id)
    {
        return await _userService.GetUserByIdAsync(id);
    }

    public async Task AddUserAsync(User user)
    {
        await _userService.AddUserAsync(user);
    }
}
