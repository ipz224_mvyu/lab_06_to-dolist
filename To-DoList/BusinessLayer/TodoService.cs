﻿using DataAccess.Repository;
using Model;
using Model.State;

namespace BusinessLayer;
// Клас, що реалізує сервіс для роботи з завданнями.
public class TodoService : ITodoService
{
    private readonly ITodoRepository _todoRepository;

    // Конструктор, який приймає репозиторій для роботи з завданнями.
    public TodoService(ITodoRepository todoRepository)
    {
        _todoRepository = todoRepository;
    }

    // Асинхронний метод для отримання завдань для певної дати та користувача.
    public async Task<IEnumerable<TodoItem>> GetTasksForDateAsync(DateTime date, int userId)
    {
        var allTasks = await _todoRepository.GetAllTasksAsync();
        // Фільтруємо завдання, щоб отримати тільки ті, які мають задану дату та належать заданому користувачу.
        return allTasks.Where(task => task.Deadline == date.Date && task.UserId == userId);
    }

    // Асинхронний метод для отримання завдання за ідентифікатором.
    public async Task<TodoItem> GetTaskByIdAsync(int id)
    {
        return await _todoRepository.GetTaskByIdAsync(id);
    }

    // Асинхронний метод для додавання нового завдання.
    public async Task AddTaskAsync(TodoItem todoItem)
    {
        await _todoRepository.AddTaskAsync(todoItem);
    }

    // Асинхронний метод для оновлення існуючого завдання.
    public async Task UpdateTaskAsync(TodoItem todoItem)
    {
        // Якщо завдання виконане, змінюємо його стан на CompletedState.
        if (todoItem.IsCompleted)
        {
            todoItem.ChangeState(new CompletedState());
        }
        // В іншому випадку, змінюємо стан на ActiveState.
        else
        {
            todoItem.ChangeState(new ActiveState());
        }

        await _todoRepository.UpdateTaskAsync(todoItem);
    }

    // Асинхронний метод для видалення завдання за ідентифікатором.
    public async Task RemoveTaskAsync(int id)
    {
        await _todoRepository.RemoveTaskAsync(id);
    }
}
