﻿using BusinessLayer.Observer;
using DataAccess;
using DataAccess.Repository;
using Model;

namespace BusinessLayer;
// Клас, що реалізує сервіс для роботи з історією та підтримує патерн Observer.
public class HistoryService : IHistoryService, IHistory
{
    // Репозиторій для роботи з історією.
    private readonly IHistoryRepository _historyRepository;

    // Список спостерігачів, які підписалися на оновлення історії.
    private readonly List<IObserver> _observers = new List<IObserver>();

    // Конструктор, який приймає репозиторій для роботи з історією.
    public HistoryService(IHistoryRepository historyRepository)
    {
        _historyRepository = historyRepository;
    }

    // Асинхронний метод для додавання історії.
    public async Task AddHistoryAsync(History history)
    {
        await _historyRepository.AddHistoryAsync(history);
        // Після додавання історії, сповіщаємо всіх спостерігачів.
        Notify(history);
    }

    // Асинхронний метод для отримання всієї історії.
    public async Task<IEnumerable<History>> GetAllHistoriesAsync()
    {
        return await _historyRepository.GetAllHistoriesAsync();
    }

    // Асинхронний метод для видалення всієї історії.
    public async Task DeleteAllHistoriesAsync()
    {
        await _historyRepository.DeleteAllHistoriesAsync();
    }

    // Метод для підписки спостерігача на оновлення історії.
    public void Attach(IObserver observer)
    {
        _observers.Add(observer);
    }

    // Метод для відписки спостерігача від оновлень історії.
    public void Detach(IObserver observer)
    {
        _observers.Remove(observer);
    }

    // Асинхронний метод для логування історії завдань.
    public async Task LogTaskHistoryAsync(string action, TodoItem task)
    {
        var history = new History
        {
            Title = action,
            Description = $"Завдання: {task.Title}, Дія: {action}",
            Date = DateTime.Now,
            UserId = task.UserId
        };

        var historyService = new HistoryService(new HistoryRepository(new AppDbContext()));
        await historyService.AddHistoryAsync(history);
    }

    // Метод для сповіщення всіх спостерігачів про нову історію.
    public async void Notify(History history)
    {
        foreach (var observer in _observers)
        {
            observer.Update(history);
        }
    }

    // Асинхронний метод для отримання історії за ідентифікатором користувача.
    public async Task<IEnumerable<History>> GetHistoryByUserIdAsync(int userId)
    {
        return (await _historyRepository.GetAllHistoriesAsync()).Where(task => task.UserId == userId);
    }
}
