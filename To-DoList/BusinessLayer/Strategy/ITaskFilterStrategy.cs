﻿using Model;
namespace BusinessLayer.Strategy;
public interface ITaskFilterStrategy
{
    IEnumerable<TodoItem> Filter(IEnumerable<TodoItem> tasks);
}
