﻿using Model;

namespace BusinessLayer;
public interface IHistoryService
{
    Task<IEnumerable<History>> GetAllHistoriesAsync();
    Task DeleteAllHistoriesAsync();
    Task AddHistoryAsync(History history);
}
