﻿using Model;

namespace BusinessLayer.Observer;
// Клас, що реалізує спостерігача для історії.
public class HistoryObserver : IObserver
{
    private readonly IHistoryService _historyService;

    // Конструктор, який може бути порожнім, якщо сервіс історії не потрібно ініціалізувати.
    public HistoryObserver() {}

    // Метод, який викликається, коли спостерігач оновлюється новими даними.
    public void Update(History history)
    {
        _historyService.AddHistoryAsync(history);
    }
}
