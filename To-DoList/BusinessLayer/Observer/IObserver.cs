﻿using Model;

namespace BusinessLayer.Observer;
public interface IObserver
{
    void Update(History history);
}
