﻿using Model.State;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model;

public class TodoItem
{
    [Key]
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Priority { get; set; }
    public DateTime? Deadline { get; set; }
    public bool IsCompleted { get; set; }
    public int UserId { get; set; }
    public User User { get; set; }

    [NotMapped]
    public ITodoState State { get; set; }

    public void ChangeState(ITodoState newState)
    {
        State = newState;
        State.Handle(this);
    }
}