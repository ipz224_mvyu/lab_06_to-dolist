﻿using System.ComponentModel.DataAnnotations;

namespace Model;

public class User
{
    [Key]
    public int Id { get; set; }
    public string Email { get; set; }
    public string PasswordHash { get; set; }
    public ICollection<TodoItem> TodoItems { get; set; }
    public ICollection<History> Histories { get; set; }

}
